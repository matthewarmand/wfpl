from urllib3.util import Retry
from requests import Session
from requests.adapters import HTTPAdapter


class FPLQuery:
    """
    _team_history_cache
    {
        team_id: {
            gameweek_id: {
                events: {
                    events response
                },
                selections: {
                    chip: "",
                    picks: {
                        picks response
                    }
                },
            },
            ...
        },
        ...
    }
    """

    def __init__(self):
        self._proxy = self.FPLProxy()
        self._team_history_cache = {}
        self._player_history_cache = {}

    def team_exists(self, team_id: int) -> bool:
        return self._proxy.team_exists(team_id)

    def get_fpl_team_info(self, team_id: int):
        return self._proxy.team_info(team_id)

    def get_gameweek_events(self):
        return self._proxy.bootstrap()["events"]

    def get_fpl_team_current_events(self, team_id: int, gameweek_id: int):
        team_id = str(team_id)
        gameweek_id = str(gameweek_id)
        if (
            team_id not in self._team_history_cache
            or "events" not in self._team_history_cache[team_id]
        ):
            if team_id not in self._team_history_cache:
                self._team_history_cache[team_id] = {}
            self._team_history_cache[team_id]["events"] = {
                str(event["event"]): event
                for event in self._proxy.team_history(team_id)["current"]
            }
        if gameweek_id not in self._team_history_cache[team_id]["events"]:
            return None
        return self._team_history_cache[team_id]["events"][gameweek_id]

    def get_team_selection(self, team_id: int, gameweek_id: int):
        team_id = str(team_id)
        gameweek_id = str(gameweek_id)
        if (
            team_id not in self._team_history_cache
            or "picks" not in self._team_history_cache[team_id]
            or gameweek_id not in self._team_history_cache[team_id]["selections"]
        ):
            picks_response = self._proxy.team_picks(team_id, gameweek_id)

            if team_id not in self._team_history_cache:
                self._team_history_cache[team_id] = {}
            if "selections" not in self._team_history_cache[team_id]:
                self._team_history_cache[team_id]["selections"] = {}

            captain = None
            vice = None
            for pick in picks_response["picks"]:
                if pick["is_captain"]:
                    captain = pick
                elif pick["is_vice_captain"]:
                    vice = pick
                if captain and vice:
                    break
            self._team_history_cache[team_id]["selections"][gameweek_id] = {
                "chip": picks_response["active_chip"],
                "captain": captain,
                "vice_captain": vice,
                "picks": picks_response["picks"],
            }
        if gameweek_id not in self._team_history_cache[team_id]["selections"]:
            return None
        return self._team_history_cache[team_id]["selections"][gameweek_id]

    def get_player_info(self, player_id: int, gameweek_id: int):
        player_id = str(player_id)
        gameweek_id = str(gameweek_id)
        if player_id not in self._player_history_cache:
            self._player_history_cache[player_id] = {}
            for h in self._proxy.player_info(player_id)["history"]:
                round_num = str(h["round"])
                if round_num not in self._player_history_cache[player_id]:
                    self._player_history_cache[player_id][round_num] = []
                self._player_history_cache[player_id][round_num].append(h)
        if gameweek_id not in self._player_history_cache[player_id]:
            return None
        return self._player_history_cache[player_id][gameweek_id]

    def get_player_gw_points(self, player_id: int, gameweek_id: int):
        results = self.get_player_info(player_id, gameweek_id)
        if results:
            return sum(result["total_points"] for result in results)
        return 0

    class FPLProxy:
        def __init__(self):
            self.base_url = "https://fantasy.premierleague.com/api"
            self.s = Session()
            retries = Retry(
                total=3,
                backoff_factor=0.1,
                status_forcelist=[502, 503, 504],
                allowed_methods={"GET"},
            )
            self.s.mount(self.base_url, HTTPAdapter(max_retries=retries))

        def bootstrap(self):
            return self.s.get(f"{self.base_url}/bootstrap-static/").json()

        def team_exists(self, team_id: int) -> bool:
            return self.s.get(f"{self.base_url}/entry/{team_id}/").status_code == 200

        def team_info(self, team_id: int):
            return self.s.get(f"{self.base_url}/entry/{team_id}/").json()

        def team_history(self, team_id: int):
            return self.s.get(f"{self.base_url}/entry/{team_id}/history/").json()

        def team_picks(self, team_id: int, gameweek_id: int):
            return self.s.get(
                f"{self.base_url}/entry/{team_id}/event/{gameweek_id}/picks/"
            ).json()

        def player_info(self, player_id: int):
            return self.s.get(f"{self.base_url}/element-summary/{player_id}/").json()
