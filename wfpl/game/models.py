from asgiref.sync import sync_to_async
from django.db import models
from django.forms import ValidationError
from django.utils.html import format_html
from django.utils.safestring import mark_safe

from game.fpl import FPLQuery


class SeasonQuerySet(models.query.QuerySet):
    def get_or_create(self, start_time: str, defaults=None, **kwargs):
        """
        Takes the deadline time of the first gameweek of the season and either
        gets or creates the season object. Will also update the start_time for
        the Season if it's been changed since creation

        :param str start_time: 2022-08-05T17:30:00Z
        :param dict defaults: fields to be used in creation but not lookup
        :param dict kwargs: arguments to be used in lookup
        """
        season_start_year = start_time[0:4]
        name = f"{season_start_year}/{int(season_start_year[2:4]) + 1}"
        if defaults is None:
            defaults = {}
        defaults["start_time"] = start_time
        defaults["nickname"] = name
        obj, created = super().get_or_create(
            name=name,
            defaults=defaults,
            **kwargs,
        )
        if obj.start_time != start_time:
            obj.start_time = start_time
            obj.save()
        return obj, created

    async def aget_or_create(self, start_time: str, defaults=None, **kwargs):
        return await sync_to_async(self.get_or_create)(
            start_time, defaults=defaults, **kwargs
        )


class SeasonManager(models.Manager):
    def get_queryset(self):
        return SeasonQuerySet(self.model)


# Created, populated entirely from API interactions
class Season(models.Model):
    name = models.CharField(max_length=7, unique=True, db_index=True)
    nickname = models.CharField(max_length=255)
    start_time = models.DateTimeField(db_index=True)
    in_progress = models.BooleanField(default=True)

    class Meta:
        get_latest_by = "start_time"
        ordering = ("-start_time",)

    objects = SeasonManager()

    def __str__(self) -> str:
        ret = f"{self.name}: {self.nickname}"
        return f"{ret} (Completed)" if not self.in_progress else ret


def validate_team_fpl_id(value: str):
    fpl = FPLQuery()
    if (
        value is None
        or not value.isdigit()
        or int(value) <= 0
        or not fpl.team_exists(value)
    ):
        raise ValidationError(
            "%(value) does not appear to be a valid and existing team ID for FPL",
            code="invalid",
            params={"value": value},
        )


# Created with FPL ID from admin screen
class Team(models.Model):
    fpl_id = models.CharField(max_length=32, validators=[validate_team_fpl_id])
    season = models.ForeignKey(Season, on_delete=models.CASCADE)
    name = models.CharField(max_length=32)
    manager_name = models.CharField(max_length=255)
    total_score = models.IntegerField(null=True)
    total_vc_score = models.IntegerField(null=True)

    class Meta:
        ordering = [
            "-season",
            "manager_name",
        ]

    def __str__(self) -> str:
        return f"{self.name} ({self.manager_name})"

    def update_metadata_from_api(self, save: bool = True):
        fpl = FPLQuery()
        api_info = fpl.get_fpl_team_info(self.fpl_id)
        self.name = str(api_info["name"])
        api_manager_name = (
            str(api_info["player_first_name"]) + " " + str(api_info["player_last_name"])
        )[0:255]
        if self.manager_name != api_manager_name:
            print(
                f"WARNING: Manager name has changed from {self.manager_name} to {api_manager_name}. Verify gwreport sort order"
            )
        self.manager_name = api_manager_name
        if save:
            self.save()

    def link_to_fpl(
        self, gameweek_id: "int | None" = None, display_format: "str | None" = None
    ) -> str:
        if (
            self.season.in_progress
            or not Season.objects.filter(in_progress=True).exists()
        ):
            if not gameweek_id:
                latest_gameweek = (
                    Gameweek.objects.filter(season=self.season)
                    .order_by("fpl_id")
                    .last()
                )
                if latest_gameweek is not None:
                    gameweek_id = latest_gameweek.fpl_id
            if gameweek_id:
                url = f"https://fantasy.premierleague.com/entry/{self.fpl_id}/event/{gameweek_id}"
            else:
                url = f"https://fantasy.premierleague.com/entry/{self.fpl_id}/history"

            return mark_safe(
                format_html(
                    '<a href="{}", target="_blank", rel="noopener noreferrer">{}</a>',
                    url,
                    (
                        display_format.format(team=self)
                        if display_format is not None
                        else self.fpl_id
                    ),
                )
            )

        return "--"


# Created, populated entirely from API interactions
class Gameweek(models.Model):
    fpl_id = models.IntegerField()
    season = models.ForeignKey(Season, on_delete=models.CASCADE)
    name = models.CharField(max_length=32)
    started = models.BooleanField(default=False)
    completed = models.BooleanField(default=False)
    global_average = models.IntegerField(null=True)
    local_average = models.DecimalField(max_digits=5, decimal_places=2, null=True)
    vc_average = models.DecimalField(max_digits=5, decimal_places=2, null=True)

    class Meta:
        unique_together = ["season", "fpl_id"]
        indexes = [
            models.Index(fields=["season", "fpl_id"]),
        ]
        ordering = ["-season", "fpl_id"]

    def __str__(self) -> str:
        return f"{self.season.name} {self.gw_id()}"

    def gw_id(self) -> str:
        return f"GW{self.fpl_id}"


# Created, populated entirely from API interactions
class GameweekScore(models.Model):
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    gameweek = models.ForeignKey(Gameweek, on_delete=models.CASCADE)
    score = models.IntegerField(null=True)
    vc_score = models.IntegerField(null=True)
    chip = models.CharField(null=True, blank=True, max_length=32)
    value = models.IntegerField(null=True)
    bank = models.IntegerField(null=True)
    transfers = models.IntegerField(null=True)
    transfers_cost = models.IntegerField(null=True)
    bench_points = models.IntegerField(null=True)

    class Meta:
        unique_together = ["gameweek", "team"]
        indexes = [models.Index(fields=["gameweek", "team"])]
        ordering = ["gameweek", "team"]

    def __str__(self) -> str:
        return f"{self.gameweek} {self.team.name}: {self.score} (VC: {self.vc_score})"


def validate_schedule(value, expected_elimination_count, start, end):
    specific_error = ""
    if value is None:
        specific_error = "Null schedule, needs to be defined"
    if not isinstance(value, list):
        specific_error = "Not a valid list (eg [1, 2, 3])"
    elif len(value) != (end + 1 - start):
        specific_error = "Wrong number of elements (need 1 for each gameweek)"
    elif sum(value) != expected_elimination_count:
        specific_error = (
            "Wrong total eliminations (need 1 fewer than total number of teams)"
        )
    return specific_error


# Created from admin screen
class Competition(models.Model):
    class Methods:
        BELOW_AVERAGE = "BELOW_AVG"
        LOWEST_X = "LOWEST_X"
        choices = [
            (BELOW_AVERAGE, "Below Average"),
            (LOWEST_X, "Lowest X"),
        ]

    class ScheduleTypes:
        BATTLE_ROYALE = "BAT_ROY"
        SPREAD_EVEN = "SPREAD"
        CUSTOM = "CUSTOM"
        choices = [
            (BATTLE_ROYALE, "Battle Royale (1 each week and the rest the last week)"),
            (SPREAD_EVEN, "Spread evenly until a final 1-on-1 head to head matchup"),
            (CUSTOM, "Custom (define your own for each week)"),
        ]

    season = models.ForeignKey(Season, on_delete=models.CASCADE)
    start_gw_fpl_id = models.PositiveIntegerField()
    end_gw_fpl_id = models.PositiveIntegerField()
    name = models.CharField(max_length=255)
    champion = models.ForeignKey(
        Team,
        on_delete=models.PROTECT,
        related_name="competitions_won",
        null=True,
        blank=True,
    )
    method = models.CharField(max_length=9, choices=Methods.choices)
    schedule_type = models.CharField(
        max_length=7, choices=ScheduleTypes.choices, null=True
    )
    schedule = models.JSONField(default=list)

    def __str__(self) -> str:
        return f"{self.name} ({self.season})"

    def teams(self):
        return Team.objects.filter(season=self.season)

    def eligible_teams(self):
        return self.teams().exclude(
            id__in=Elimination.objects.filter(competition=self).values_list(
                "team__id", flat=True
            )
        )

    def needs_attention(self):
        return CompetitionWarning.objects.filter(competition=self).exists()

    def compute_schedule(self):
        if self.method == self.Methods.BELOW_AVERAGE:
            return
        # inclusive count (subtraction +1) but ignoring the final (-1)
        week_count_less_final = self.end_gw_fpl_id - self.start_gw_fpl_id
        # total eliminations in the competition
        elimination_count = self.teams().count() - 1
        if self.schedule_type == self.ScheduleTypes.BATTLE_ROYALE:
            self.schedule = []
            # Append 1's for every week leading up to the final
            for _ in range(week_count_less_final):
                self.schedule.append(1)
            # Append the rest of the eliminations all in the final week
            self.schedule.append(elimination_count - week_count_less_final)
        elif self.schedule_type == self.ScheduleTypes.SPREAD_EVEN:
            base, extra = divmod(elimination_count - 1, week_count_less_final)
            self.schedule = list(
                reversed([base + (i < extra) for i in range(week_count_less_final)])
            )
            self.schedule.append(1)
        elif self.schedule_type == self.ScheduleTypes.CUSTOM:
            error_message = validate_schedule(
                self.schedule,
                elimination_count,
                self.start_gw_fpl_id,
                self.end_gw_fpl_id,
            )
            if error_message:
                warning, created = CompetitionWarning.objects.get_or_create(
                    competition=self,
                    warning_type=CompetitionWarning.NeedsAttentionTypes.CUSTOM_SCHEDULE_NEEDS_FIXING,
                    defaults={"additional_info": error_message},
                )
                if not created:
                    warning.additional_info = error_message
                    warning.save()
        else:
            raise NotImplementedError(
                f"Schedule computation needed for {self.schedule_type}"
            )


# Created, populated entirely from API interactions
class Playoff(models.Model):
    competition = models.ForeignKey(Competition, on_delete=models.CASCADE)
    source_gameweek = models.ForeignKey(
        Gameweek, on_delete=models.PROTECT, related_name="spawned_playoff"
    )
    target_gameweek = models.ForeignKey(
        Gameweek, on_delete=models.PROTECT, related_name="resolved_playoff"
    )
    teams = models.ManyToManyField(Team)

    def __str__(self):
        return f"{self.id}: Playoff for {self.competition.name} {self.source_gameweek.gw_id()} between {[t.manager_name for t in self.teams.all()]} to be resolved in {self.target_gameweek.gw_id()}"


# Created, populated entirely from API interactions
class Elimination(models.Model):
    competition = models.ForeignKey(Competition, on_delete=models.CASCADE)
    gameweek = models.ForeignKey(Gameweek, on_delete=models.PROTECT)
    team = models.ForeignKey(Team, on_delete=models.PROTECT)
    from_playoff = models.ForeignKey(
        Playoff, on_delete=models.CASCADE, null=True, blank=True
    )

    def __str__(self):
        ret = f"{self.id}: {self.team.manager_name} eliminated from {self.competition.name} in {self.gameweek.gw_id()}"
        if self.from_playoff:
            ret += f" via playoff {self.from_playoff.id} from {self.from_playoff.source_gameweek.gw_id()}"
        return ret


# Created, populated entirely from API interactions; resolved through user action
class CompetitionWarning(models.Model):
    class NeedsAttentionTypes:
        MANUAL_CHAMPION_SELECTION = "MANUAL_CHAMP_SEL"
        CUSTOM_SCHEDULE_NEEDS_FIXING = "CUSTOM_SCHEDULE_FIX"
        choices = [
            (MANUAL_CHAMPION_SELECTION, "Need Manual Champion Selection"),
            (CUSTOM_SCHEDULE_NEEDS_FIXING, "Custom Schedule Needs Fixing"),
        ]

    competition = models.ForeignKey(Competition, on_delete=models.CASCADE)
    warning_type = models.CharField(max_length=20, choices=NeedsAttentionTypes.choices)
    additional_info = models.CharField(max_length=255)

    def __str__(self):
        return f"{self.get_warning_type_display()}: {self.additional_info}"
