from collections import defaultdict
from datetime import datetime
from decimal import Decimal, localcontext

from django.db.models.query import (  # pylint: disable=unused-import # type hints
    QuerySet,
)
from tqdm import tqdm

from game.fpl import FPLQuery
from game.models import (
    Competition,
    CompetitionWarning,
    Elimination,
    Gameweek,
    GameweekScore,
    Playoff,
    Season,
    Team,
)


def update_team_metadata(
    disable_progress_bar: bool = True, teams: "QuerySet | list[Team]" = None
):
    if teams:
        teams = teams.filter(season__in_progress=True)
    else:
        in_progress_season = Season.objects.get(in_progress=True)
        if in_progress_season:
            teams = Team.objects.filter(season=in_progress_season)
    if teams and teams.exists():
        for team in tqdm(teams, desc="Team Metadata", disable=disable_progress_bar):
            if disable_progress_bar:
                print(f"Updating metadata for {team}...")
            team.update_metadata_from_api()


def update_game(disable_progress_bar: bool = True, force_update: bool = False):
    fpl = FPLQuery()
    gameweek_events = fpl.get_gameweek_events()

    season_start_time = gameweek_events[0]["deadline_time"]

    current_season, created = Season.objects.get_or_create(season_start_time)
    if created:
        print(
            f"INFO::: The {current_season.name} season has begun! Time to add teams etc"
        )

    teams = Team.objects.filter(season=current_season)
    found_unfinished_gameweek = False
    for event in tqdm(gameweek_events, desc="Gameweeks", disable=disable_progress_bar):
        completed = event["finished"] and event["data_checked"]
        found_unfinished_gameweek = found_unfinished_gameweek or not completed
        if datetime.now() >= datetime.strptime(
            event["deadline_time"], "%Y-%m-%dT%H:%M:%SZ"
        ):
            gameweek, _ = Gameweek.objects.get_or_create(
                fpl_id=event["id"],
                season=current_season,
                defaults={"name": event["name"]},
            )
            gameweek.started = True

            if disable_progress_bar:
                print(f"Updating team scores for {gameweek.name}...")
            for team in tqdm(
                teams, desc="Teams", leave=False, disable=disable_progress_bar
            ):
                if disable_progress_bar:
                    print(f"Updating GW{gameweek.fpl_id} score for {team}...")
                gameweek_score, gws_created = GameweekScore.objects.get_or_create(
                    team=team, gameweek=gameweek
                )

                if gws_created or ((not gameweek.completed) or force_update):
                    team_info = fpl.get_fpl_team_current_events(
                        team.fpl_id, gameweek.fpl_id
                    )
                    gameweek_score.score = (
                        team_info["points"] - team_info["event_transfers_cost"]
                    )
                    gameweek_score.value = team_info["value"]
                    gameweek_score.bank = team_info["bank"]
                    gameweek_score.transfers = team_info["event_transfers"]
                    gameweek_score.transfers_cost = team_info["event_transfers_cost"]
                    gameweek_score.bench_points = team_info["points_on_bench"]
                    selection = fpl.get_team_selection(team.fpl_id, gameweek.fpl_id)
                    vc_score = fpl.get_player_gw_points(
                        selection["vice_captain"]["element"], gameweek.fpl_id
                    )
                    if selection["chip"] == "3xc":
                        vc_score *= 3

                    gameweek_score.chip = selection["chip"]
                    gameweek_score.vc_score = vc_score
                    gameweek_score.save()
                    team.total_score = team_info["total_points"]
                    team.save()

            gameweek.global_average = event["average_entry_score"]
            scores = gameweek.gameweekscore_set.all()
            if scores.exists():
                with localcontext() as ctx:
                    ctx.prec = 5
                    gameweek.local_average = Decimal(
                        sum(scores.values_list("score", flat=True))
                    ) / Decimal(scores.count())
                    gameweek.vc_average = Decimal(
                        sum(scores.values_list("vc_score", flat=True))
                        / Decimal(scores.count())
                    )
            gameweek.completed = completed
            gameweek.save()
            if gameweek.completed:
                update_competitions(
                    Competition.objects.filter(season=current_season), gameweek
                )

    if disable_progress_bar:
        print("Updating team vc totals...")
    if teams.exists():
        for team in tqdm(teams, desc="Teams VC totals", disable=disable_progress_bar):
            if disable_progress_bar:
                print(f"Updating team vc totals for {team}...")
            scores = team.gameweekscore_set.all()
            team.total_vc_score = sum(scores.values_list("vc_score", flat=True))
            team.save()
    if not found_unfinished_gameweek:
        current_season.in_progress = False
        current_season.save()
        print(f"INFO::: The {current_season.name} season has ended!")
    for warning in CompetitionWarning.objects.filter(
        competition__season=current_season
    ):
        print(f"WARNING for {warning.competition}::: {warning}")


class MethodUpdater:
    def update(self, competition, gameweek_scores, gameweek):
        raise NotImplementedError()


class BelowAverageUpdater(MethodUpdater):
    def update(self, competition, gameweek_scores, gameweek):
        total_candidates = gameweek_scores.count()
        eliminated = gameweek_scores.filter(
            score__lt=gameweek.global_average,
        ).values_list("team", flat=True)
        if total_candidates == eliminated.count():
            CompetitionWarning.objects.get_or_create(
                competition=competition,
                warning_type=CompetitionWarning.NeedsAttentionTypes.MANUAL_CHAMPION_SELECTION,
                additional_info="Gameweek {gameweek.fpl_id} eliminated all candidates",
            )
        else:
            for team in eliminated:
                Elimination.objects.get_or_create(
                    competition=competition, gameweek=gameweek, team_id=team
                )


class LowestXUpdater(MethodUpdater):
    def update(self, competition, gameweek_scores, gameweek):
        try:
            playoff = Playoff.objects.get(
                competition=competition, target_gameweek=gameweek
            )
        except Playoff.DoesNotExist:
            pass
        else:
            position = playoff.source_gameweek.fpl_id - competition.start_gw_fpl_id
            remaining_eliminations = (
                competition.schedule[position]
                - Elimination.objects.filter(
                    competition=competition, gameweek=playoff.source_gameweek
                ).count()
            )
            if remaining_eliminations > 0:
                self._update_helper(
                    competition,
                    remaining_eliminations,
                    GameweekScore.objects.filter(
                        gameweek=gameweek, team__in=playoff.teams.all()
                    ),
                    gameweek,
                    playoff.source_gameweek,
                    playoff,
                )

        position = gameweek.fpl_id - competition.start_gw_fpl_id
        remaining_eliminations = (
            competition.schedule[position]
            - Elimination.objects.filter(
                competition=competition, gameweek=gameweek
            ).count()
        )
        if remaining_eliminations > 0:
            self._update_helper(
                competition, remaining_eliminations, gameweek_scores, gameweek
            )

    def _update_helper(
        self,
        competition,
        remaining_eliminations,
        gameweek_scores,
        current_gameweek,
        source_gameweek=None,
        playoff=None,
    ):
        score_map = defaultdict(list)
        for gws in gameweek_scores.order_by("score"):
            score_map[gws.score].append(gws)
        while remaining_eliminations > 0:
            candidates = score_map.pop(min(score_map.keys()))
            if len(candidates) <= remaining_eliminations:
                for gws in candidates:
                    Elimination.objects.get_or_create(
                        competition=competition,
                        gameweek=source_gameweek or current_gameweek,
                        team=gws.team,
                        from_playoff=playoff,
                    )
                remaining_eliminations -= len(candidates)
            elif current_gameweek.fpl_id != 38:
                playoff, _ = Playoff.objects.get_or_create(
                    competition=competition,
                    source_gameweek=current_gameweek,
                    target_gameweek=Gameweek.objects.get_or_create(
                        season=current_gameweek.season,
                        fpl_id=current_gameweek.fpl_id + 1,
                    )[0],
                )
                playoff.teams.set([c.team for c in candidates])
                break
            else:
                CompetitionWarning.objects.get_or_create(
                    competition=competition,
                    warning_type=CompetitionWarning.NeedsAttentionTypes.MANUAL_CHAMPION_SELECTION,
                    additional_info="Tie, and unable to set a playoff for next week",
                )
                break


def update_competitions(competitions, gameweek):
    UPDATERS = {
        Competition.Methods.BELOW_AVERAGE: BelowAverageUpdater,
        Competition.Methods.LOWEST_X: LowestXUpdater,
    }
    for comp in competitions:
        if (
            comp.start_gw_fpl_id <= gameweek.fpl_id <= comp.end_gw_fpl_id
            or Playoff.objects.filter(
                competition=comp, target_gameweek=gameweek
            ).exists()
        ) and comp.champion is None:
            gameweek_scores = GameweekScore.objects.filter(
                team__in=comp.eligible_teams(),
                gameweek=gameweek,
            )
            UPDATERS[comp.method]().update(comp, gameweek_scores, gameweek)
            eligible_teams = comp.eligible_teams()
            if eligible_teams.count() == 1:
                comp.champion = eligible_teams.first()
                comp.save()
