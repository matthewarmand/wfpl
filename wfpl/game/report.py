from collections import defaultdict

from game.models import Gameweek, Season, Playoff, Elimination, Team


def gameweek_report(gameweek_ids, season_name=None, output_top=None):
    if output_top is None:
        output_top = 5
    season = (
        Season.objects.get(name=season_name) if season_name else Season.objects.latest()
    )

    not_first = False
    for gwid in gameweek_ids:
        if not_first:
            print("\n")
        not_first = True
        gw = Gameweek.objects.filter(season=season, fpl_id=gwid).first()
        if not gw or not gw.completed:
            print(f"Gameweek {gwid} not completed, skipping")
            continue
        values = []
        values.append(str(gw.global_average))
        scores = gw.gameweekscore_set.all().order_by("team__manager_name")
        for score in scores:
            values.extend(
                [
                    str(score.score),
                    str(score.vc_score) + ("*" if score.chip == "3xc" else ""),
                ]
            )
        print(f"Gameweek {gwid}:  " + ",".join(values))
        for playoff in Playoff.objects.filter(source_gameweek=gw):
            print(
                f"Spawned new {playoff.competition.name} playoff for {playoff.target_gameweek.gw_id()} for {', '.join([t.manager_name for t in playoff.teams.all()])}"
            )
        comp_elim_map = defaultdict(list)
        for elim in Elimination.objects.filter(gameweek=gw).order_by("competition"):
            comp_elim_map[elim.competition.name].append(elim)
        for comp_name, elims in comp_elim_map.items():
            print(
                f"{comp_name} Eliminations: "
                + ", ".join(
                    f"{elim.team.manager_name}"
                    + (
                        f" in playoff in {elim.from_playoff.target_gameweek.gw_id()}"
                        if elim.from_playoff
                        else ""
                    )
                    for elim in elims
                )
            )
        for playoff in Playoff.objects.filter(target_gameweek=gw):
            print(
                f"Resolved Playoff {playoff.id} for {playoff.competition.name} between {', '.join([t.manager_name for t in playoff.teams.all()])}: Eliminated {', '.join([e.team.manager_name for e in Elimination.objects.filter(from_playoff=playoff)])}"
            )
    print(
        f"\nOverall Score Top {output_top}:\n"
        + "\n".join(
            f"    {index+1}. {t.manager_name} ({t.total_score})"
            for index, t in enumerate(
                Team.objects.filter(season=season).order_by("-total_score")[:output_top]
            )
        )
    )
    print(
        f"VC Score Top {output_top}:\n"
        + "\n".join(
            f"    {index+1}. {t.manager_name} ({t.total_vc_score})"
            for index, t in enumerate(
                Team.objects.filter(season=season).order_by("-total_vc_score")[
                    :output_top
                ]
            )
        )
    )
