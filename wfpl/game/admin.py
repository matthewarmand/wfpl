from collections import defaultdict
from datetime import datetime, timezone
from django.contrib import admin
from django.db.models.functions import Lower
from django.forms import ModelForm, ValidationError
from django.urls import reverse
from django.utils.html import format_html
from django.utils.safestring import mark_safe

from game.models import (
    Competition,
    CompetitionWarning,
    Elimination,
    Gameweek,
    GameweekScore,
    Season,
    Team,
    validate_schedule,
    validate_team_fpl_id,
)


class SeasonLinkMixin:
    @admin.display(ordering="season", description="season")
    def season_link(self, obj) -> str:
        return format_html(
            '<a href="{}">{}</a>',
            reverse("admin:game_season_change", args=[obj.season.id]),
            str(obj.season),
        )


class TeamLinkMixin:
    @admin.display(ordering="team", description="team")
    def team_link(self, obj, team_name_only=False) -> str:
        if not isinstance(obj, Team):
            obj = obj.team
        return format_html(
            '<a href="{}">{}</a>',
            reverse("admin:game_team_change", args=[obj.id]),
            obj.name if team_name_only else str(obj),
        )


class GameweekLinkMixin:
    @admin.display(ordering="gameweek", description="gameweek")
    def gameweek_link(self, obj) -> str:
        if not isinstance(obj, Gameweek):
            obj = obj.gameweek
        return format_html(
            '<a href="{}">{}</a>',
            reverse("admin:game_gameweek_change", args=[obj.id]),
            obj.gw_id(),
        )


class ReadOnlyAdminMixin:
    def has_delete_permission(
        self, request, obj=None  # pylint: disable=unused-argument # django compat
    ) -> bool:
        return False

    def has_add_permission(
        self, request  # pylint: disable=unused-argument # django compat
    ) -> bool:
        return False

    def has_change_permission(
        self, request, obj=None  # pylint: disable=unused-argument # django compat
    ) -> bool:
        return False


class TeamAdminForm(ModelForm):
    class Meta:
        model = Team
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if "season" in self.fields:
            self.fields["season"].queryset = Season.objects.filter(in_progress=True)


@admin.register(Team)
class TeamAdmin(SeasonLinkMixin, GameweekLinkMixin, admin.ModelAdmin):
    form = TeamAdminForm
    list_display = (
        "name",
        "manager_name",
        "link_to_fpl",
        "season_link",
        "total_score",
        "total_vc_score",
    )
    list_filter = ("season",)
    search_fields = ("name", "manager_name")

    def get_fields(
        self, request, obj=None
    ):  # pylint: disable=unused-argument # django compat
        if obj:
            return (
                "name",
                "manager_name",
                "link_to_fpl",
                "season",
                "total_score",
                "total_vc_score",
                "gameweek_scores",
            )
        return ("fpl_id",)

    def get_readonly_fields(
        self, request, obj=None
    ):  # pylint: disable=unused-argument # django compat
        if obj:
            return (
                "name",
                "manager_name",
                "link_to_fpl",
                "season",
                "total_score",
                "total_vc_score",
                "gameweek_scores",
            )
        return ()

    def clean_fpl_id(self):
        value = self.cleaned_data["fpl_id"]
        validate_team_fpl_id(value)
        return value

    def save_model(self, request, obj, form, change):
        obj.season = Season.objects.latest()
        obj.update_metadata_from_api(save=False)
        super().save_model(request, obj, form, change)

    def has_add_permission(self, request):
        return (
            super().has_add_permission(request)
            and datetime.now(timezone.utc) < Season.objects.latest().start_time
        )

    def has_delete_permission(self, request, obj=None):
        return super().has_delete_permission(request, obj) and (
            not obj or datetime.now(timezone.utc) < obj.season.start_time
        )

    def gameweek_scores(self, obj: Team) -> str:
        scores = obj.gameweekscore_set.all()
        html = "<table><thead><tr><th>Gameweek</th><th>Score</th><th>VC</th><th>Chip</th><th>Value</th><th>Bank</th><th>Transfers</th><th>Transfers Cost</th><th>Bench Points</th></tr></thead><tbody>"
        for s in scores:
            html += f"<tr><td>{self.gameweek_link(s)}</td><td>{s.score}</td><td>{s.vc_score}</td><td>{s.chip or '--'}</td><td>{s.value}</td><td>{s.bank}</td><td>{s.transfers}</td><td>{s.transfers_cost}</td><td>{s.bench_points}</td></tr>"
        html += "</tbody></table>"
        return mark_safe(html)


@admin.register(Season)
class SeasonAdmin(GameweekLinkMixin, admin.ModelAdmin):
    list_display = ("name", "nickname", "start_time", "in_progress")
    list_filter = ("in_progress",)
    search_fields = ("name", "nickname")
    readonly_fields = ("name", "start_time", "in_progress", "global_scores")
    fields = ("nickname",) + readonly_fields

    def has_delete_permission(
        self, request, obj=None  # pylint: disable=unused-argument # django compat
    ) -> bool:
        return False

    def has_add_permission(
        self, request  # pylint: disable=unused-argument # django compat
    ) -> bool:
        return False

    def global_scores(self, obj: Season) -> str:
        gameweeks = obj.gameweek_set.all()
        html = "<table><thead><tr><th>Gameweek</th><th>Global Average</th><th>Local Average</th></tr></thead><tbody>"
        for g in gameweeks:
            html += f"<tr><td>{self.gameweek_link(g)}</td><td>{g.global_average if g.global_average is not None else '--'}</td><td>{g.local_average}</td></tr>"
        html += "</tbody></table>"
        return mark_safe(html)


@admin.register(Gameweek)
class GameweekAdmin(
    ReadOnlyAdminMixin, SeasonLinkMixin, TeamLinkMixin, admin.ModelAdmin
):
    list_display = (
        "gw_id",
        "season_link",
        "completed",
        "global_average",
        "local_average",
    )
    list_filter = ("season", "completed")
    fields = (
        "fpl_id",
        "season",
        "completed",
        "global_average",
        "local_average",
        "scores",
    )
    readonly_fields = fields

    def scores(self, obj: Gameweek) -> str:
        scores = obj.gameweekscore_set.all()
        html = "<table><thead><tr><th>Team</th><th>Manager</th><th>View on FPL</th><th>Score</th><th>VC</th><th>Chip</th><th>Value</th><th>Bank</th><th>Transfers</th><th>Transfer Cost</th><th>Bench Points</th></tr></thead><tbody>"
        for s in scores:
            html += f"<tr><td>{self.team_link(s, team_name_only=True)}</td><td>{s.team.manager_name}</td><td>{s.team.link_to_fpl(gameweek_id=obj.fpl_id)}</td><td>{s.score}</td><td>{s.vc_score}</td><td>{s.chip or '--'}</td></tr>"
        html += "</tbody></table>"
        return mark_safe(html)


@admin.register(GameweekScore)
class GameweekScoreAdmin(
    ReadOnlyAdminMixin, TeamLinkMixin, GameweekLinkMixin, admin.ModelAdmin
):
    list_display = (
        "__str__",
        "team_link",
        "team_link_to_fpl",
        "gameweek_link",
        "score",
        "vc_score",
        "chip",
        "value",
        "transfers",
        "transfers_cost",
        "bench_points",
    )
    list_filter = ("gameweek__season", "chip", "gameweek__fpl_id", "team")
    fields = (
        "team",
        "team_link_to_fpl",
        "gameweek",
        "score",
        "vc_score",
        "chip",
        "value",
        "transfers",
        "transfers_cost",
        "bench_points",
    )
    readonly_fields = fields

    @admin.display(description="Team Link To FPL")
    def team_link_to_fpl(self, obj: GameweekScore) -> str:
        return obj.team.link_to_fpl(gameweek_id=obj.gameweek.fpl_id)


class CompetitionNeedsAttentionListFilter(admin.SimpleListFilter):
    title = "Status"

    parameter_name = "status"

    def lookups(
        self, request, model_admin
    ):  # pylint: disable=unused-argument # django compat
        return [("needs_attention", "Needs Attention")]

    def queryset(
        self, request, queryset
    ):  # pylint: disable=unused-argument # django compat
        if self.value() == "needs_attention":
            return queryset.filter(
                id__in=[w.competition.id for w in CompetitionWarning.objects.all()]
            )
        return queryset


class CompetitionAdminForm(ModelForm):
    class Meta:
        model = Competition
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance and "champion" in self.fields:
            self.fields["champion"].queryset = self.instance.eligible_teams()
        if "season" in self.fields:
            self.fields["season"].queryset = Season.objects.filter(in_progress=True)

    def _validate_gw_id(self, fieldname):
        value = self.cleaned_data[fieldname]
        if value < 1 or value > 38:
            raise ValidationError(
                "%(fieldname) must be between 1 and 38.",
                code="invalid",
                params={"fieldname": fieldname},
            )
        return value

    def clean_start_gw_fpl_id(self):
        return self._validate_gw_id("start_gw_fpl_id")

    def clean_end_gw_fpl_id(self):
        return self._validate_gw_id("end_gw_fpl_id")

    def clean(self):
        start = self.cleaned_data["start_gw_fpl_id"]
        end = self.cleaned_data["end_gw_fpl_id"]
        if start > end:
            raise ValidationError(
                "Start Gameweek must be before End Gameweek", code="invalid"
            )

        if "schedule" in self.cleaned_data:
            value = self.cleaned_data["schedule"]
            error = validate_schedule(
                value, self.instance.teams().count() - 1, start, end
            )
            if error:
                raise ValidationError(
                    f"Invalid schedule; must contain {end + 1 - start} elements (currently {len(value)}) and total {self.instance.teams().count() - 1} eliminations (curently {sum(value)})",
                    code="invalid",
                )
            CompetitionWarning.objects.filter(
                competition=self.instance,
                warning_type=CompetitionWarning.NeedsAttentionTypes.CUSTOM_SCHEDULE_NEEDS_FIXING,
            ).delete()
        if (
            "champion" in self.cleaned_data
            and self.cleaned_data["champion"] is not None
        ):
            CompetitionWarning.objects.filter(
                competition=self.instance,
                warning_type=CompetitionWarning.NeedsAttentionTypes.MANUAL_CHAMPION_SELECTION,
            ).delete()


@admin.register(Competition)
class CompetitionAdmin(
    TeamLinkMixin, SeasonLinkMixin, GameweekLinkMixin, admin.ModelAdmin
):
    form = CompetitionAdminForm
    list_display = (
        "name",
        "season_link",
        "start_gw_fpl_id",
        "end_gw_fpl_id",
        "status",
        "champion_link",
    )
    list_filter = ("season", CompetitionNeedsAttentionListFilter)

    def get_ordering(self, request):  # pylint: disable=unused-argument # django compat
        return ["season", "start_gw_fpl_id", Lower("name")]

    def get_readonly_fields(
        self, request, obj=None
    ):  # pylint: disable=unused-argument # django compat
        fields = ()
        if obj:
            fields += ("eliminations", "method")
            if obj.needs_attention():
                fields += ("needs_attention_messages",)

            starting_gw = Gameweek.objects.filter(
                season=obj.season, fpl_id=obj.start_gw_fpl_id
            ).first()
            if starting_gw and starting_gw.completed:
                fields += ("start_gw_fpl_id", "end_gw_fpl_id", "schedule_type")
        if (
            not obj
            or not obj.needs_attention()
            or not CompetitionWarning.objects.filter(
                competition=obj,
                warning_type=CompetitionWarning.NeedsAttentionTypes.MANUAL_CHAMPION_SELECTION,
            ).exists()
        ):
            fields += ("champion",)

        return fields

    def get_fields(
        self, request, obj=None
    ):  # pylint: disable=unused-argument # django compat
        fields = (
            "season",
            "name",
            "start_gw_fpl_id",
            "end_gw_fpl_id",
            "method",
        )
        if obj:
            fields += ("champion",)
            if obj.method == Competition.Methods.LOWEST_X:
                fields += ("schedule_type",)
                if obj.schedule_type == Competition.ScheduleTypes.CUSTOM:
                    fields += ("schedule",)
            if obj.needs_attention():
                fields += ("needs_attention_messages",)
            fields += ("eliminations",)

        return fields

    def save_model(self, request, obj, form, change):
        if obj.schedule_type:
            obj.compute_schedule()
        super().save_model(request, obj, form, change)

    @admin.display(description="needs urgent attention")
    def needs_attention_messages(self, obj: Competition) -> str:
        return mark_safe(
            '<span style="color: red">'
            + "<br />".join(
                str(warning)
                for warning in CompetitionWarning.objects.filter(competition=obj)
            )
            + "</span>"
        )

    @admin.display(description="Status")
    def status(self, obj: Competition) -> bool:
        return not CompetitionWarning.objects.filter(competition=obj).exists()

    status.boolean = True

    def eliminations(self, obj: Competition) -> str:
        eliminations = Elimination.objects.filter(competition=obj).order_by(
            "gameweek__fpl_id", "team__manager_name"
        )
        elim_map = defaultdict(list)
        for e in eliminations:
            elim_map[e.gameweek.fpl_id].append(e)
        last_gw_id = 0
        html = f"<table><thead><tr><th>Gameweek</th>{'<th># Eliminations</th>' if obj.method == Competition.Methods.LOWEST_X else ''}<th>Team</th><th>Manager</th><th>View on FPL</th></tr></thead><tbody>"
        for gw_id in range(obj.start_gw_fpl_id, obj.end_gw_fpl_id + 1):
            elims = elim_map[gw_id]
            if elims:
                for e in elims:
                    html += "<tr>"
                    if e.gameweek.fpl_id != last_gw_id:
                        html += f'<td rowspan="{len(elim_map[e.gameweek.fpl_id])}">{self.gameweek_link(e.gameweek)}</td>'
                        if obj.method == Competition.Methods.LOWEST_X:
                            html += f'<td rowspan="{len(elim_map[e.gameweek.fpl_id])}">{obj.schedule[gw_id - obj.start_gw_fpl_id]}</td>'
                    html += f"<td>{self.team_link(e, team_name_only=True)}</td><td>{e.team.manager_name}</td><td>{e.team.link_to_fpl(gameweek_id=e.gameweek.fpl_id)}</td>"
                    html += "</tr>"
                    last_gw_id = e.gameweek.fpl_id
            elif obj.method == Competition.Methods.LOWEST_X:
                html += "<tr>"
                html += f"<td>GW{gw_id}</td>"
                html += f"<td>{obj.schedule[gw_id - obj.start_gw_fpl_id] if obj.schedule else ''}</td>"
                html += "<td>--</td><td>--</td><td>--</td>"
                html += "</tr>"

        html += "</tbody></table>"
        return mark_safe(html)

    @admin.display(description="Champion")
    def champion_link(self, obj: Competition) -> str:
        return self.team_link(obj.champion) if obj.champion is not None else "--"
