from django.core.management import BaseCommand

from game.models import Gameweek
from game.report import gameweek_report
from game.updater import update_game, update_team_metadata


class Command(BaseCommand):
    help = "Use this on an ad-hoc or scheduled basis to update the game data"

    def add_arguments(self, parser):
        parser.add_argument(
            "--game",
            action="store_true",
            help="Update game data",
        )
        parser.add_argument(
            "--team-metadata",
            action="store_true",
            help="Update team metadata",
        )
        parser.add_argument(
            "--no-progress",
            action="store_true",
            help="Don't show the progress bar",
        )
        parser.add_argument(
            "--force-update",
            action="store_true",
            help="Re-update all data, instead of just new data",
        )
        parser.add_argument(
            "--no-report",
            action="store_true",
            help="Don't print the report of newly completed weeks",
        )
        parser.add_argument(
            "--top",
            type=int,
            required=False,
            help="for report; output top N for relevant competitions. Defaults to 5",
        )

    def handle(
        self, *args, **options
    ):  # pylint: disable=unused-argument # django compat
        if options["team_metadata"]:
            update_team_metadata(disable_progress_bar=bool(options["no_progress"]))
        if options["game"]:
            if not options["no_report"]:
                previously_completed_gw_ids = {
                    gw.id for gw in Gameweek.objects.filter(completed=True)
                }

            update_game(
                disable_progress_bar=bool(options["no_progress"]),
                force_update=bool(options["force_update"]),
            )

            if not options["no_report"]:
                newly_completed_gw_ids = {
                    gw.fpl_id
                    for gw in Gameweek.objects.filter(completed=True).exclude(
                        id__in=previously_completed_gw_ids
                    )
                }
                if newly_completed_gw_ids:
                    print("")
                    gameweek_report(newly_completed_gw_ids, output_top=options["top"])
