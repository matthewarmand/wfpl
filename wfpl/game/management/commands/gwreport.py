from django.core.management import BaseCommand

from game.report import gameweek_report


class Command(BaseCommand):
    help = "Use this to output a gameweek report for pasting into the spreadsheet"

    def add_arguments(self, parser):
        parser.add_argument(
            "gameweek_ids",
            nargs="+",
            type=int,
            help="any number of gameweek ids to output stats for. Incomplete gameweeks will be skipped",
        )
        parser.add_argument(
            "--season",
            type=str,
            required=False,
            help="optional season to specify. Defaults to current season",
        )
        parser.add_argument(
            "--top",
            type=int,
            required=False,
            help="output top N for relevant competitions. Defaults to 5",
        )

    def handle(
        self, *args, **options
    ):  # pylint: disable=unused-argument # django compat
        gameweek_report(options["gameweek_ids"], options["season"], options["top"])
